#
# Be sure to run `pod spec lint DMLazyScrollView.podspec' to ensure this is a
# valid spec.
# For details see: https://github.com/CocoaPods/CocoaPods/wiki/The-podspec-format
#
Pod::Spec.new do |s|
  s.name         = "SMPageControl"
  s.version      = "0.0.1"
  s.summary      = "A drop in replacement for UIPageControl with a slew of additional customization options."
  s.homepage     = "https://github.com/Spaceman-Labs/SMPageControl"
  s.license      = {
    :type => 'MIT',
    :text => <<-LICENSE
Copyright (C) 2012 by Spaceman Labs

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
     LICENSE
  }

  # Specify the authors of the library, with email addresses. You can often find
  # the email addresses of the authors by using the SCM log. E.g. $ git log
  #
  s.author       = { "Spaceman Labs" => "spaceman@spacemanlabs.com" }
  s.source       = { :git => "https://github.com/Spaceman-Labs/SMPageControl.git", :commit => "38eb16e6d5b45d8030a8a40967b2708a2de65546" }
  s.platform     = :ios

  s.source_files = 'SMPageControl.{h,m}'
  s.requires_arc = true
end
