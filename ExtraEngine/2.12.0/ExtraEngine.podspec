Pod::Spec.new do |s|
  s.name = 'ExtraEngine'
  s.version = '2.12.0'
  s.summary = "Moteur d'application iPhone/iPhone."
  s.description = "Moteur d'application iPhone/iPhone."
  s.homepage = 'http://www.google.com/'
  s.author = 'Joseph Maarek'
  s.requires_arc = true
  s.source = { :git => "git@bitbucket.org:maarek_j/extraengine.git", :tag => 'v2.12.0' }
  s.platform = :ios
  s.ios.deployment_target = '7.0'

  s.subspec 'Core' do |sub|
    sub.source_files = "ExtraEngine/Classes/**/*.{h,m}"
    sub.resources = [ "ExtraEngine/Resources/extraengine.bundle", "ExtraEngine/Classes/**/*.xib" ]

    sub.dependency 'AFNetworking', '~> 2.0'
    sub.dependency 'SVProgressHUD'
    sub.dependency 'SVPullToRefresh'
    sub.dependency 'PKRevealController'
    sub.dependency 'PrettyKit'
    sub.dependency 'DMLazyScrollView'
    sub.dependency 'SMPageControl'
    sub.dependency 'KVOBlockBinding'
    sub.dependency 'MWFeedParser'

    sub.framework = 'Security'
  end

  s.subspec 'ScoopIt' do |sub|
    sub.source_files = "ExtraEngine/Additional/ScoopIt/Classes/*.{h,m}"
    sub.dependency 'ExtraEngine/Core'
  end

  s.subspec 'Twitter' do |sub|
    sub.source_files = "ExtraEngine/Additional/Twitter/Classes/*.{h,m}"
    sub.dependency 'ExtraEngine/Core'
    sub.dependency 'STTwitter', '>=0.0.4'
  end

  s.subspec 'Socialize' do |sub|
    sub.source_files = "ExtraEngine/Additional/Socialize/Classes/*.{h,m}"
    sub.dependency 'ExtraEngine/Core'
    sub.dependency 'Socialize'
  end

  s.subspec 'Flurry' do |sub|
    sub.source_files = "ExtraEngine/Additional/Flurry/Classes/*.{h,m}"
    sub.dependency 'ExtraEngine/Core'
    sub.dependency 'FlurrySDK'
  end
  
  s.subspec 'Drupal' do |sub|
    sub.source_files = "ExtraEngine/Additional/Drupal/Classes/**/*.{h,m}"
    sub.resources = [ "ExtraEngine/Additional/Drupal/**/*.xib" ]
    sub.dependency 'ExtraEngine/Core'
  end

  s.subspec 'Mublish' do |sub|
    sub.source_files = "ExtraEngine/Additional/Mublish/Classes/**/*.{h,m}"
    sub.resources = [ "ExtraEngine/Additional/Mublish/**/*.xib" ]
    sub.dependency 'ExtraEngine/Core'
    sub.dependency 'ExtraEngine/Drupal'
  end
end
