Pod::Spec.new do |s|
  s.name = 'GetSocialize'
  s.version = '2.7.2'
  s.summary = 'The Socialize SDK for iOS.'
  s.description = 'The Socialize SDK provides a simple set of classes and methods built upon the Socialize REST API.'
  s.homepage = 'http://www.getsocialize.com/'
  s.author = 'GetSocialize a sharethis company.'

  s.source = { :http => 'https://s3.amazonaws.com/socialize-sdk-distribution/socialize-sdk-ios-2.7.2.zip', :flatten => true }
  s.platform = :ios
  s.ios.deployment_target = '5.0'

  s.source_files = "Socialize.embeddedframework/Socialize.framework/Versions/A/Headers/*.h"
  s.resource = "Socialize.embeddedframework/Resources/*"

  s.preserve_paths = "Socialize.embeddedframework"
  s.header_dir = 'Socialize'

  s.frameworks = 'CoreLocation', 'MapKit', 'MessageUI', 'QuartzCore', 'CoreGraphics', 'AdSupport', 'Socialize'

  s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '"$(PODS_ROOT)/GetSocialize/" "$(PODS_ROOT)/GetSocialize/Socialize.embeddedframework"', 'OTHER_LDFLAGS' => '-all_load -ObjC' }
end