Pod::Spec.new do |s|
  s.name         = "MbAdSdk"
  s.version      = "1.0.0"
  s.summary      = "Mister Bell est la régie mobile à la performance numéro 1 en France et l'un des leaders en Europe."
  s.homepage     = "http://www.misterbell.com/"

  s.license      = {
    :type => '',
    :text => ''
  }
  
  s.author       = { "Joseph Maarek" => "josephmaarek@gmail.com" }
  s.source       = { :git => "git@bitbucket.org:maarek_j/mbadsdk.git", :tag => 'v1.0.0' }

  s.platform     = :ios, '5.0'
  s.requires_arc = true

  s.source_files = '*.h'  
  s.preserve_paths = 'libmbAdSdk.a'

  s.library   = 'mbAdSdk'
  s.xcconfig = { 'LIBRARY_SEARCH_PATHS' => '"$(PODS_ROOT)/MbAdSdk"' }
  
  s.framework = 'iAd'
end
