#
# Be sure to run `pod spec lint DMLazyScrollView.podspec' to ensure this is a
# valid spec.
# For details see: https://github.com/CocoaPods/CocoaPods/wiki/The-podspec-format
#
Pod::Spec.new do |s|
  s.name         = "KVOBlockBinding"
  s.version      = "0.0.1"
  s.summary      = "Use blocks to observe Objective-C properties using KVO."
  s.homepage     = "https://github.com/rayh/kvo-block-binding"
  s.license      = {
    :type => 'MIT',
    :text => <<-LICENSE
     LICENSE
  }

  # Specify the authors of the library, with email addresses. You can often find
  # the email addresses of the authors by using the SCM log. E.g. $ git log
  #
  s.author       = { "Ray Yamamoto Hilton" => "ray@wirestorm.net" }
  s.source       = { :git => "https://github.com/rayh/kvo-block-binding.git", :commit => "676e71d7b35046db383ea9b8f2d05bc07cb81270" }
  s.platform     = :ios

  # ――― MULTI-PLATFORM VALUES ――――――――――――――――――――――――――――――――――――――――――――――――― #


  # targets.
  #
  # s.ios.deployment_target = '5.0'
  # s.osx.deployment_target = '10.7'

  # A list of file patterns which select the source files that should be
  # added to the Pods project. If the pattern is a directory then the
  # path will automatically have '*.{h,m,mm,c,cpp}' appended.
  #
  # Alternatively, you can use the FileList class for even more control
  # over the selected files.
  # (See http://rake.rubyforge.org/classes/Rake/FileList.html.)
  #
  s.source_files = 'KVOBlockBinding/*.{h,m}'
  s.requires_arc = true
end
