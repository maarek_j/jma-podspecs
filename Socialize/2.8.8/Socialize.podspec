Pod::Spec.new do |s|
  s.name = 'Socialize'
  s.version = '2.8.8'
  s.summary = 'The Socialize SDK for iOS.'
  s.description = 'The Socialize SDK provides a simple set of classes and methods built upon the Socialize REST API.'
  s.homepage = 'http://www.getsocialize.com/'
  s.author = 'Socialize a sharethis company.'

  s.source = { :git => "https://maarek_j@bitbucket.org/maarek_j/socialize-sdk-ios-framework.git", :tag => 'v2.8.8' }
  s.platform = :ios
  s.ios.deployment_target = '5.0'

  s.source_files = ["Socialize.embeddedframework/Socialize.framework/Versions/A/Headers/*.h", "Pinterest.embeddedframework/Pinterest.framework/Versions/A/Headers/*.h"]
  s.resources = ["Socialize.embeddedframework/Resources/*", "Pinterest.embeddedframework/Resources/*"]

  s.preserve_paths = ["Socialize.embeddedframework", 'Pinterest.embeddedframework']
  s.header_dir = 'Socialize'

  s.frameworks = 'CoreLocation', 'MapKit', 'MessageUI', 'QuartzCore', 'CoreGraphics', 'AdSupport', 'Socialize', 'Pinterest'

  s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '"$(PODS_ROOT)/Socialize/" "$(PODS_ROOT)/Socialize/Socialize.embeddedframework" "$(PODS_ROOT)/Socialize/Pinterest.embeddedframework"', 'OTHER_LDFLAGS' => '-all_load -ObjC' }
end