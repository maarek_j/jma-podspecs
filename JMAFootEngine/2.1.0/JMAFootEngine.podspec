Pod::Spec.new do |s|
  s.name          = "JMAFootEngine"
  s.version       = "2.1.0"
  s.summary       = "Un moteur d'application de foot basé sur ExtraEngine."
  s.homepage      = "https://bitbucket.org/maarek_j/jmafootextraengine/overview"

  s.license       = {
    :type => '',
    :text => ''
  }

  s.author        = { "Joseph Maarek" => "josephmaarek@gmail.com" }
  s.source        = { :git => "git@bitbucket.org:maarek_j/jmafootextraengine.git", :tag => 'v2.1.0' }

  s.platform      = :ios, '6.1'
  s.requires_arc  = true

  s.source_files  = 'JMAFootEngine/Classes/**/*.{h,m}'
  s.resources     = [ "JMAFootEngine/Resources/footengine.bundle", "JMAFootEngine/Classes/**/*.xib" ]

  s.dependency "ExtraEngine", '~> 2.4'
  s.dependency "MbAdSdk"
  s.dependency "Underscore.m"
  s.dependency "Facebook-iOS-SDK"
  s.dependency "JMATellAFriend"
  s.dependency "Google-AdMob-Ads-SDK"
  s.dependency "Parse", '= 1.2.17'

  s.frameworks    = "Parse"

  s.xcconfig      = { 'HEADER_SEARCH_PATHS' => '"$(PODS_ROOT)/Parse/Parse.framework/Headers"' }
end
