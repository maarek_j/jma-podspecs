Pod::Spec.new do |s|
  s.name         = "JMAFootEngine"
  s.version      = "1.0.9"
  s.summary      = "Un moteur d'application de foot basé sur ExtraEngine."
  s.homepage     = "https://bitbucket.org/maarek_j/jmafootextraengine/overview"

  s.license      = {
    :type => '',
    :text => ''
  }

  s.author       = { "Joseph Maarek" => "josephmaarek@gmail.com" }
  s.source       = { :git => "git@bitbucket.org:maarek_j/jmafootextraengine.git", :tag => 'v1.0.9' }

  s.platform     = :ios, '6.0'
  s.requires_arc = true

  s.source_files = 'JMAFootEngine/Classes/**/*.{h,m}'
  s.resources = [ "JMAFootEngine/Resources/footengine.bundle", "JMAFootEngine/Classes/**/*.xib" ]

  s.dependency "ExtraEngine", '~> 0.5.3'
  s.dependency "MbAdSdk"
  s.dependency "Underscore.m"
  s.dependency "UIGlossyButton"
  s.dependency "Facebook-iOS-SDK"
  s.dependency "JMATellAFriend"
end
