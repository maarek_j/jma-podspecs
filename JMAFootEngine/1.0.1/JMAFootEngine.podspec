Pod::Spec.new do |s|
  s.name         = "JMAFootEngine"
  s.version      = "1.0.1"
  s.summary      = "Un moteur d'application de foot basé sur ExtraEngine."
  s.homepage     = "https://bitbucket.org/maarek_j/jmafootextraengine/overview"

  s.license      = {
    :type => '',
    :text => ''
  }
  
  s.author       = { "Joseph Maarek" => "josephmaarek@gmail.com" }
  s.source       = { :git => "https://maarek_j@bitbucket.org/maarek_j/jmafootextraengine.git", :tag => 'v1.0.1' }

  s.platform     = :ios, '5.0'
  s.requires_arc = true

  s.source_files = 'JMAFootEngine/Classes/**/*.{h,m}', 'JMAFootEngine/Vendors/mbAdSDK/*.h'
  s.resources = [ "JMAFootEngine/Resources/**/*", "JMAFootEngine/Classes/**/*.xib" ]
  
  s.preserve_paths = 'JMAFootEngine/Vendors/mbAdSDK/libmbAdSdk.a'

  s.library   = 'mbAdSdk'
  s.xcconfig = { 'LIBRARY_SEARCH_PATHS' => '"$(PODS_ROOT)/JMAFootEngine/JMAFootEngine/Vendors/mbAdSDK"' }
  
  s.dependency "ExtraEngine"
  s.dependency "Underscore.m"
  s.dependency "UIGlossyButton"
  
  s.framework = 'iAd'
end
