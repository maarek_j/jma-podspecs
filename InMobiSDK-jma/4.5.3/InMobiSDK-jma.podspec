Pod::Spec.new do |s|
  s.name      = "InMobiSDK-jma"
  s.version   = "4.5.3"
  s.summary   = "InMobi iOS SDK."
  s.homepage  = "https://www.inmobi.com"
  s.license   = {
    :type => "proprietary",
    :text => "               Copyright (C) InMobi. All Right Reserved.\n"
  }

  s.authors   = "InMobi"
  s.source = { :http => "https://www.dropbox.com/s/3wncjjbpex7a79f/InMobi_iOS_SDK-4.5.3.zip?dl=1"}
 
  s.source_files = "InMobi-iOS-SDK-4.5.3/Libs/*.h"
  s.public_header_files = "InMobi-iOS-SDK-4.5.3/Libs/*.h"
  s.preserve_paths = "InMobi-iOS-SDK-4.5.3/Libs/*.a"
  
  s.libraries = "InMobi-4.5.3", "z", "sqlite3.0"

  s.frameworks = "AudioToolbox", "AVFoundation", "CoreTelephony", "EventKit", "EventKitUI", "MessageUI", "MediaPlayer", "Security", "SystemConfiguration"

  s.weak_frameworks =  "AdSupport", "Social", "StoreKit"

  s.vendored_libraries = "InMobi-iOS-SDK-4.5.3/Libs/libInMobi-4.5.3.a"

  s.requires_arc = false
end