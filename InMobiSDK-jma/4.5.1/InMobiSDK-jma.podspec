Pod::Spec.new do |s|
  s.name      = "InMobiSDK-jma"
  s.version   = "4.5.1"
  s.summary   = "InMobi iOS SDK."
  s.homepage  = "https://www.inmobi.com"
  s.license   = {
    :type => "proprietary",
    :text => "               Copyright (C) InMobi. All Right Reserved.\n"
  }

  s.authors   = "InMobi"
  s.source = { :http => "https://dl.inmobi.com/SDK/InMobi_iOS_SDK.zip"}
 
  s.source_files = "InMobi-iOS-SDK-4.5.1/Libs/*.h"
  s.public_header_files = "InMobi-iOS-SDK-4.5.1/Libs/*.h"
  s.preserve_paths = "InMobi-iOS-SDK-4.5.1/Libs/*.a"
  
  s.libraries = "InMobi-4.5.1", "z", "sqlite3.0"

  s.frameworks = "AudioToolbox", "AVFoundation", "CoreTelephony", "EventKit", "EventKitUI", "MessageUI", "MediaPlayer", "Security", "SystemConfiguration"

  s.weak_frameworks =  "AdSupport", "Social", "StoreKit"

  s.xcconfig = {
    "OTHER_LDFLAGS" => "-ObjC",
    "LIBRARY_SEARCH_PATHS" => "${PODS_ROOT}/InMobiSDK-jma/InMobi-iOS-SDK-4.5.1/Libs"
  }

  s.requires_arc = false
end